﻿import urllib2
from BeautifulSoup import BeautifulSoup, UnicodeDammit
import cloudinary
from cloudinary import uploader
import MySQLdb
from optparse import OptionParser
import re
import random
import cStringIO
from PIL import Image

PROVIDER_MTHAI = 1
PROVIDER_SANOOK = 2
PROVIDER_KAPOOK = 3

def find_best_image_src( soup ):
    manual_image_set = set()
    best_image_size = 400*400
    best_image = None
    for img in soup.findAll("img"):
        size = 0
        try:
            size = int(img['width']) * int(img['height'])
        except (KeyError, ValueError):
            if img['src'] in manual_image_set:
                continue

            manual_image_set.add(img['src'])
            print("Manually downloading image: " + img['src'])
            # Need to download the image to see how big it actually is
            try:
                pil_imgdata = urllib2.urlopen( img['src'] ).read()
                pil_img = Image.open( cStringIO.StringIO(pil_imgdata) )
                width, height = pil_img.size
                size = width * height
                print("Manual image size found to be: " + str(size))
            except urllib2.HTTPError:
                # Some images return error 403, forbidden?
                pass
            except ValueError:
                # Probably just a fubar image, no worries
                pass

        if size > best_image_size:
            best_image = img
            best_image_size = size

    if best_image is None:
        print( "No suitable image found!" )
        return None

    print best_image['src']
    return best_image['src']

def does_page_already_exist( conn, page_url ):
    cur = conn.cursor()
    rowcount = cur.execute("""select id from pages where page_url = %s;""", (page_url,) )
    return rowcount > 0


def scrape_detail_1(conn, page_url, id):
    print( "Scraping: " + page_url )

    page = urllib2.urlopen(page_url).read()
    # page = UnicodeDammit( page, isHTML=True )
    soup = BeautifulSoup(page)

    # TB TODO - Should only compile this one time
    re_description = re.compile("^description$", re.I)

    print( soup.findAll( attrs={"name":re_description} ))
    description = soup.findAll( attrs={"name":re_description} )[0]['content'] 

    # Some descriptions have retarded long http text, so just ditch them
    if description.find("http") >= 0:
        print( "Description is lame, aborting" )
        return

    best_image_src = find_best_image_src(soup)
    if best_image_src is None:
        return

    # TB TODO - Error checking. Missing image, image too small, missing description, etc 

    cloudinary_result = uploader.upload( best_image_src, width=640, height=480, crop="fill", gravity="face")
    print cloudinary_result['url']

    cur = conn.cursor()
    cur.execute("""update pages set is_added=true, added_timestamp=UNIX_TIMESTAMP(NOW()), original_img_url=%s, fixed_img_url=%s, description=%s where id=%s;""", (best_image_src, cloudinary_result['url'], description, id) )

def scrape_detail_sanook(conn, page_url, id):
    scrape_detail_1(conn, clean_url, id)

def scrape_detail_mthai(conn, page_url, id):
    scrape_detail_1(conn, page_url, id)
    
def scrape_detail_kapook(conn, page_url, id):
    scrape_detail_1(conn, page_url, id)

def get_random_unadded_url( conn, days_back ):
    cur = conn.cursor()
    rowcount = cur.execute("""select id, page_url, provider_id from pages where scraped_timestamp > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL %s DAY));""", (days_back,) )
    if rowcount > 0:
        row = cur.fetchall()[ random.choice( range(rowcount) ) ]
        return row
    return None

def scrape_random_detail(conn):
    row = None
    days_back = 0
    while row is None:
        days_back += 1
        if days_back == 4:
            print( "ERROR: No new rows were found!" )    
            return
        row = get_random_unadded_url( conn, days_back )

    id, page_url, provider_id = ( row[0], row[1], row[2] )
    if provider_id == PROVIDER_MTHAI:
        scrape_detail_mthai(conn, page_url, id)
    elif provider_id == PROVIDER_SANOOK:
        scrape_detail_sanook(conn, page_url, id)
    elif provider_id == PROVIDER_KAPOOK:
        scrape_detail_kapook(conn, page_url, id)
    else:
        raise Exception( "unknown provider_id!" )

def insert_new_pages( provider_id, url_set ):
    sql = 'insert into pages(provider_id, scraped_timestamp, page_url, is_added) values '
    first = True
    vals = []
    for url in url_set:
        if does_page_already_exist(conn, url):
            continue
        if not first:
            sql += ','
        first = False
        sql += '(%s, UNIX_TIMESTAMP(NOW()), %s, false )'
        # TB TODO - Printing the URL can cause terrible things to happen
        # print( "adding " + url )
        vals.append( provider_id )
        vals.append( url )

    if len(vals) == 0:
        return 

    # print sql
    cur = conn.cursor()
    cur.execute(sql, vals)

def scrape_home_mthai( conn ):
    print( "scrape_home_mthai" )
    url_set = set()
    provider_id = PROVIDER_MTHAI

    page = urllib2.urlopen("http://mthai.com").read()
    soup = BeautifulSoup(page)
    items = soup.findAll("div", "item")
    for item in items:
        # The anchor tag should have an image link
        if len(item.findAll("img")) == 0:
            continue
        url = item.findAll("a")[0]['href']
        if url.find( "mthai.com" ) == -1:
            continue
        if url.find( "video.mthai.com" ) >= 0:
            continue
        url_set.add( url )

    insert_new_pages( provider_id, url_set )


def scrape_home_sanook( conn ):
    print( "scrape_home_sanook" )
    url_set = set()
    provider_id = PROVIDER_SANOOK

    page = urllib2.urlopen("http://sanook.com").read()
    soup = BeautifulSoup(page)
    items = soup.findAll("article")
    for item in items:
        # url = item.findAll("a")[0]['href']
        links = item.findAll("a")
        if len(links) == 0:
            continue
        url = links[0]['href']
        # The anchor tag should have an image link
        if len(item.findAll("img")) == 0:
            continue
        if url.find( "sanook.com" ) == -1:
            continue
        # Remove all the long thai crap text at the end of the URL
        m = re.search(r".*sanook\.com/[\d]+", url)
        if m is None:
            continue
        url = m.group(0)
        url_set.add( url )

    insert_new_pages( provider_id, url_set )

def scrape_home_kapook( conn ):
    print( "scrape_home_kapook" )
    url_set = set()
    provider_id = PROVIDER_KAPOOK
    page = urllib2.urlopen("http://kapook.com").read()
    soup = BeautifulSoup(page)
    for article_group in soup.findAll("article"):
        for item in article_group.findAll("li"):
            # The anchor tag should have an image link
            if len(item.findAll("img")) == 0:
                continue
            try:
                url = item.findAll("a")[0]['href']
            except IndexError:
                continue
            if url.find( "kapook.com" ) == -1:
                continue 
            url_set.add( url )

    insert_new_pages( provider_id, url_set )


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-d", "--database", dest="db", help="mysql database ip address")
    parser.add_option("-u", "--username", dest="username", help="mysql database username")
    parser.add_option("-p", "--password", dest="password", help="mysql database password")
    (options, args) = parser.parse_args()
    if options.db == None or options.username == None or options.password == None:
        parser.error("Missing required params, use -h for help")

    #conn = MySQLdb.connect(host=options.db, db='thaijai', user=options.username, passwd=options.password, charset='utf8')
    print options.db
    print options.username
    print options.password
    #conn = MySQLdb.connect(host='127.0.0.1', db='thaijai', user='thaijai', passwd='thaijai', charset='utf8', use_unicode=True)
    conn = MySQLdb.connect(host=options.db, db='thaijai', user=options.username, passwd=options.password, charset='utf8', use_unicode=True)
    conn.autocommit(True)

    cloudinary.config(  cloud_name = 'tbak42',    api_key = '724963242153859', api_secret = 'bmoyMtTTGpY2_9caHrEtW7aUcK0'  )
    scrape_home_mthai(conn)
    scrape_home_sanook(conn)
    # Generally ugly pictures, weird content?
    # scrape_home_kapook(conn)
    scrape_random_detail( conn )
    print( "buye!")
