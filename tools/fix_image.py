﻿from PIL import Image
from PIL import ImageFilter
from PIL import ImageDraw
import numpy as np
import cv2
import cv2.cv as cv
import urllib, cStringIO

TARGET_WIDTH = 640
TARGET_HEIGHT = 480


def try_crop_face( full_image, scale_factor, cimg ):
    cascade = cv2.CascadeClassifier('haarcascades/haarcascade_frontalface_alt.xml')

    gray = cv2.cvtColor(cimg, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    faces = cascade.detectMultiScale(gray)
    print(faces)
 
    if len(faces) > 0:
        best_face = None
        best_face_size = 0

        print 'face detected!'
        draw = ImageDraw.Draw(full_image)
        for i in faces:
            i = [ int(round(a * scale_factor)) for a in i ]
            draw.rectangle( [ i[0], i[1], i[0]+i[2], i[1]+i[3] ] , fill=128 )
            if i[2] * i[3] > best_face_size:
                best_face = i[:]
                best_face_size = i[2] * i[3]

        full_image.show()
        desired_left = best_face[0] + best_face[2]/2 - TARGET_WIDTH/2
        desired_right = best_face[0] + best_face[2]/2 + TARGET_WIDTH/2
        desired_top = best_face[1] + best_face[3]/2 - TARGET_HEIGHT/2
        desired_bottom = best_face[1] + best_face[3]/2 + TARGET_HEIGHT/2
        width, height = full_image.size
        print( best_face )
        x = 0
        y = 0
        if width > TARGET_WIDTH:
            if desired_left < 0:
                x = 0
            elif desired_right > width:
                x = width - TARGET_WIDTH
            else:
                x = desired_left
                
        if height > TARGET_HEIGHT:
            if desired_top < 0:
                y = 0
            elif desired_bottom > height:
                y = height - TARGET_HEIGHT
            else:
                y = desired_top 

        print str(x)
        print str(y)

        return (x,y,x+TARGET_WIDTH, y+TARGET_HEIGHT)

    return None

# This will potentially crop onto busy but unimportant areas
# Probably best to just crop to center...?
def try_crop_features( full_image ):
    edge_image = im.filter(ImageFilter.FIND_EDGES)
    # edge_image.show()

def try_crop_center( full_image ):
    width, height = full_image.size
    x = 0
    y = 0
    if width > TARGET_WIDTH:
        x = width/2 - TARGET_WIDTH/2
    if height > TARGET_HEIGHT:
        y = height/2 - TARGET_HEIGHT/2


    return( x, y, x+TARGET_WIDTH, y+TARGET_HEIGHT )

def try_crop( full_image, scale_factor, cimg ):
    crop_coords = try_crop_face( full_image, scale_factor, cimg )
    if crop_coords is not None:
        fixed_image = full_image.crop( crop_coords )
    else: 
        crop_coords = try_crop_center( full_image ) 
        fixed_image = full_image.crop( crop_coords )
    return fixed_image

def get_full_image( im ):
    width, height = im.size
    scale_factor = 0
    if width < TARGET_WIDTH:
        scale_factor = float(TARGET_WIDTH) / width
    if height < TARGET_HEIGHT:
        if float(TARGET_HEIGHT) / height > scale_factor:
            scale_factor = float(TARGET_HEIGHT) / height

    new_size = ( int(round(width * scale_factor)), int(round(height * scale_factor)) )
    full_image = im.resize( new_size, Image.BICUBIC )        
    return full_image, scale_factor

def fix_url( image_url ):

    # TB TODO - Assign unique ID? 
    filename = "temp.dat"

    imgdata = urllib.urlopen(image_url).read()
    tempfile = open( filename, "wb" )
    tempfile.write(imgdata)
    tempfile.close()

    # Resize the image so that it is at least as big as 640x480
    # Then chop off rows or columns so that the image is exactly 640x480

    im = Image.open(filename)
    full_image, scale_factor = get_full_image(im)

    cimg = cv2.imread( filename )
    fixed_image = try_crop( full_image, scale_factor, cimg )

    fixed_image.show()
    
def fix_file( filename ):

    # TB TODO - Generate long random name for image
    # TB TODO - Copy orig file to /media/original_image/XYZ
    # TB TODO - Copy fixed file to /media/fixed_image/XYZ
    # TB TODO - Return name of image?

    im = Image.open(filename)
    full_image, scale_factor = get_full_image(im)

    cimg = cv2.imread( filename )
    fixed_image = try_crop( full_image, scale_factor, cimg )

    fixed_image.show()

if __name__ == "__main__":
    #fix_url( "http://news.mthai.com/wp-content/uploads/2013/03/283.jpg" )
    fix_file( "test1.jpg" )
    #fix_file( "test2.jpg" )
    #fix_file( "test3.jpg" )
    #fix_file( "test4.jpg" )
    #fix_file( "test5.jpg" )