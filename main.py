from functools import wraps
from os.path import exists
from jinja2.exceptions import TemplateNotFound
import time
import datetime
import flask
import MySQLdb
from PIL import Image

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = flask.request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

app = flask.Flask(__name__)

# TB TODO - Thai symbols!
@app.template_filter('timedelta')
def format_timedelta(value):
    delta_sec = time.time() - value
    delta_min = int(delta_sec // 60)
    if delta_min < 2:
        return "1m"
    elif delta_min < 60:
        return str(delta_min) + "m"
    elif delta_min < 1440:
        return str(int(delta_min//60)) + "h"
    elif delta_min < 43200:
        return str(int(delta_min//1440)) + "d"
    elif delta_min < 525599:
        return str(int(delta_min//43200)) + "mth"



@app.errorhandler(404)
def page_not_found(error):
    return '<h2>Error 404</h2><br/><br/>This page does not exist.<br/><br/><a href="/">Click here to return to the home page</a>.', 404

def get_pages( start_point, quantity ):
    conn = MySQLdb.connect(host='192.168.1.44', db='thaijai', user='thaijai', passwd='thaijai', charset='utf8', use_unicode=True)
    # conn = MySQLdb.connect(host='127.0.0.1', db='thaijai', user='thaijai', passwd='thaijai', charset='utf8', use_unicode=True)
    cur = conn.cursor()
    cur.execute("""select p.page_url, p.fixed_img_url, p.description, p.added_timestamp, r.name, r.web_site from pages p, providers r where p.provider_id = r.id and is_added=true order by p.added_timestamp desc limit %s,%s;""", (start_point, quantity,) )
    rows = cur.fetchall()
    pages = []
    for r in rows:
        # TB TODO WTFFFF - On prod, I need to decode this string or it dies... On local, it dies if I DO decode it...
        # page = { 'page_url':r[0], 'fixed_img_url':r[1], 'description':r[2].decode('utf-8'), 'added_timestamp':r[3], 'provider_name':r[4], 'provider_web_site':r[5] }
        page = { 'page_url':r[0], 'fixed_img_url':r[1], 'description':r[2], 'added_timestamp':r[3], 'provider_name':r[4], 'provider_web_site':r[5] }
        pages.append(page)
    return pages

@app.route('/api/get_pages/<int:start_point>')
def page_get_pages(start_point):
    pages = get_pages( start_point, 9 )
    pass 

@app.route('/')
def page_home():
    pages = get_pages( 0, 30 )

    # 3 pages per row
    rows = [pages[i:i+3] for i in range(0, len(pages), 3)]

    context = { 'rows': rows }
    return flask.render_template('index.html', **context )

# TB - If apache is set up correctly, this should not be used.
@app.route('/media/<path:filename>')
def ghetto_media_server(filename):
    # return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)
    return flask.send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=False)

if __name__ == '__main__':
    print "*** Starting"

    # This one is not externally available
    # app.run(debug=True, use_reloader=False)
    app.config['UPLOAD_FOLDER'] = "D:/p2/thaijai/media/"
    # app.run(debug=True, use_reloader=True)

    # This one is externally available
    #app.run(host='0.0.0.0', debug=False, use_reloader=False)
    app.run(host='0.0.0.0', debug=True, use_reloader=True)
